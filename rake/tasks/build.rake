require 'rake'
require 'rake/task'

build_target = nil
app_root = "#{File.dirname(__FILE__)}/../.."
sdk = ENV.fetch('BUILD_SDK') { 'iphonesimulator' }
ENV['APP'] = "#{app_root}/build/Release-#{sdk}/TTL.app"

desc 'Verify xcode exists'
task :verify_xcode_binary_prerequisits do
  unless OS.which('xcodebuild')
    raise 'Please install Xcode before executing these tests'
  end
end

desc 'Set build target'
task :set_build_target do
  `cd #{app_root}`
  output = `xcodebuild -list`
  info = output.split("\n")
  info.map!(&:strip)
  build_target = info[info.index('Targets:') + 1]
end

desc 'Build app'
task :build_app do
  `cd #{app_root}`
  `rm -rf build`
  `xcodebuild -configuration Release -target #{build_target} -sdk #{sdk}`
end

desc 'Build app for automated testing and execute tests'
task execute_automation: [:verify_xcode_binary_prerequisits,
                          :set_build_target, :build_app] do
  `cd #{app_root}`
  sh 'bundle exec rake rubocop'
  sh 'bundle install'
  sh 'bundle exec cucumber'
end
