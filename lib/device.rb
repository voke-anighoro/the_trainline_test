# The Device class loads its configuration from devices.yaml
# and returns objects
class Device < Settingslogic
  source File.join(PROJECT_ROOT, 'devices.yml')
  namespace ENV['DEVICE'] ||= 'iphone'
  Device['host'] ||= 'localhost'
  def start_server
    # Do not bother if the device is running on a different host
    return unless host.to_s == 'localhost'
    unless OS.which('appium')
      raise 'Appium binary not found. Please install appium and add to PATH'
    end
    # Spawn a new process and run the appium server
    pid = Process.spawn("appium --port #{Device.port}",
                        out: '/dev/null', err: '/dev/null')

    # Detach the spawned process
    ENV['APPIUM_PID'] = pid.to_s
    Process.detach pid
    # Give the appium process sometime to start
    sleep(5)
  end

  def stop_server
    # Kill appium server
    Process.kill('KILL', ENV['APPIUM_PID'].to_i) if ENV['APPIUM_PID']
  end
end
