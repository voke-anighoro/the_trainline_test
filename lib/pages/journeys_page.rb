module Pages
  # An extension of the base page modelling the journeys result view page
  class JourneysPage < Pages::BasePage
    sections :journeys, :xpath, '//UIAApplication[1]/UIAWindow[2]/UIATableView[1]/UIATableCell' do
      element :departure_label, :id, 'Dep:'
      element :arrival_label, :id, 'Arr:'
      element :departure_time, :xpath, '//UIAStaticText[3]'
      element :arrival_time, :xpath, '//UIAStaticText[4]'
      element :journey_duration, :xpath, '//UIAStaticText[5]'
      element :departure_station, :xpath, '//UIAStaticText[6]'
      element :arrival_station, :xpath, '//UIAStaticText[7]'
      element :price, '//UIAStaticText[8]'
    end
  end
end
