module Pages
  # An extension of the base page reflecting the date picker modal
  class DatePickerPage < Pages::BasePage
    section :date_picker, :xpath, '//UIAWindow[2]/UIAPicker[1]' do
      element :date, :xpath, '//UIAPickerWheel[1]'
      element :hour, :xpath, '//UIAPickerWheel[2]'
      element :minute, :xpath, '//UIAPickerWheel[3]'
      element :am_or_pm, :xpath, '//UIAPickerWheel[4]'
    end

    def select(date:, hour:, am_or_pm:)
      select_value_for_key(key: 'date', value: date)
      select_value_for_key(key: 'hour', value: hour)
      select_value_for_key(key: 'am_or_pm', value: am_or_pm)
      navigation_bar.done.click
    end

    def select_value_for_key(key:, value:)
      date_picker.send(key).set(value) unless date_picker.send(key).text == value
    end
  end
end
