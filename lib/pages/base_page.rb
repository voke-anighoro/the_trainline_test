module Pages
  # Base page object for all The Train Line app views
  class BasePage < SitePrism::Page
    section :alert, :xpath, '//UIAAlert[1]' do
      elements :messages, :xpath, '//UIAStaticText'
      element :ok, :id, 'OK'

      def message
        messages.map(&:text).join(' ')
      end
    end
    section :navigation_bar, :xpath,
            '//UIAApplication[1]/UIAWindow[2]/UIANavigationBar[1]' do
      element :done, :id, 'Done'
      element :back, :id, 'Back'
      element :back_button_text, :xpath, '//UIAButton[1]'
      element :heading, :xpath, '//UIAStaticText[1]'
      element :faq, :id, 'FAQ'
      element :search, :id, 'Search'
    end
  end
end
