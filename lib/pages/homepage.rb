module Pages
  # An extension of the base page modelling the homepage view
  class HomePage < Pages::BasePage
    element :origin, :xpath, '//UIAWindow[2]/UIATextField[1]'
    element :destination, :xpath, '//UIAWindow[2]/UIATextField[2]'
    section :journey_options, :xpath, '//UIAWindow[2]/UIASegmentedControl[1]' do
      element :one_way, :id, 'One way'
      element :open_return, :id, 'Open return'
      element :return, :id, 'Return'
    end
    element :departure, :xpath, '//UIAWindow[2]/UIAButton[2]'
    element :arrival, :xpath, '//UIAWindow[2]/UIAButton[3]'
  end
end
