# Dumping module for helper methods
module GenericHelper
  # Returns how far a jump in time we are looking to make
  def get_jump(count:, option:)
    option.strip! if option
    count = 1 unless count
    jump = fetch_jump_metrics(option: option)
    count.to_i * jump
  end

  # Returns a jump metric based on how much we are looking to leap in time
  # Defaults to an hour
  def fetch_jump_metrics(option:)
    case option
    when 'second', 'seconds'
      1
    when 'minute', 'minutes'
      60
    when 'day', 'days'
      24 * 60 * 60
    when 'week', 'weeks'
      7 * 24 * 60 * 60
    when 'month', 'months'
      4 * 7 * 24 * 60 * 60
    when 'year', 'years'
      12 * 4 * 24 * 60 * 60
    else
      60 * 60
    end
  end
end
World(GenericHelper)
