# Cucumber world module for all page related activity that is used
# globally around our cucumber framework
module PagesWorld
  include ::Pages

  def homepage
    @current_page = HomePage.new
  end

  def journeys_page
    @current_page = JourneysPage.new
  end

  def done_page
    @current_page = DonePage.new
  end

  def date_picker_page
    @current_page = DatePickerPage.new
  end
end
