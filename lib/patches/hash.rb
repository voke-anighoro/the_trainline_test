# A hash extension to include the compact method
# which removes empty items from a hash
class Hash
  def compact
    delete_if { |_k, v| v.nil? }
  end
end
