Capybara.register_driver :appium do |app|
  caps = generate_device_caps
  appium_lib_options = {
    server_url: "http://#{Device.host}:#{Device.port}/wd/hub"
  }
  all_options = {
    appium_lib:  appium_lib_options,
    caps:        caps
  }
  Appium::Capybara::Driver.new(app, all_options)
end

def generate_device_caps
  os = Device.os
  os_version = Device.os_version
  device = Device.full_name
  set_local_device_caps(os: os, os_version: os_version,
                        device: device)
end

def set_local_device_caps(os:, os_version:, device:)
  caps = {}
  caps[:deviceName] = device
  caps[:platformName] = os
  caps[:platformVersion] = os_version
  caps[:app] = ENV['APP']
  caps.compact
end
