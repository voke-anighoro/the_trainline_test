# This class holds all world related modules and includes them
# in a clean fashion
module GlobalWorld
  include PagesWorld
end
World(GlobalWorld)
