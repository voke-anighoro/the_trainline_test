#import "JourneyDateViewController.h"

@interface JourneyDateViewController ()
@property (weak, nonatomic) IBOutlet UIDatePicker *dateTimePicker;
@property (nonatomic) NSString *title;
@end

@implementation JourneyDateViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.navigationItem.title = self.title;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)doneAndNavigateBack:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(handleDateTime:)])
        [self.delegate handleDateTime:[self.dateTimePicker date]];
    [self.navigationController popViewControllerAnimated:YES];
}
@end
