#import <Foundation/Foundation.h>

@protocol SendDateTimeProtocol <NSObject>
@required
- (void) handleDateTime:(NSDate*) date;
@end
