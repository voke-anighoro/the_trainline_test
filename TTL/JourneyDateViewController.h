#import <UIKit/UIKit.h>
#import "SendDateTimeProtocol.h"

@interface JourneyDateViewController : UIViewController
{
    id <SendDateTimeProtocol> _delegate;
}
@property (nonatomic,strong) id delegate;
@end
