#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UITextField *origin;
@property (weak, nonatomic) IBOutlet UITextField *destination;
@property (weak, nonatomic) IBOutlet UIButton *departureDateTimeButton;
@property (weak, nonatomic) IBOutlet UIButton *arrivalDateTimeButton;
@property (weak, nonatomic) IBOutlet UISegmentedControl *journeyTypeSelector;
@property (retain, nonatomic) UIDatePicker *departureDatePicker;
@property (retain, nonatomic) UIDatePicker *arrivalDatePicker;
@property (nonatomic) BOOL isOutward;
@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    NSDateFormatter *formatter;
    NSString        *dateString;
    
    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"EEE dd MMM yyyy hh:mm aaa"];
    
    dateString = [formatter stringFromDate:[self roundDateToCeiling15Minutes]];
    
    [self.departureDateTimeButton setTitle:dateString forState:UIControlStateNormal];
    [self.arrivalDateTimeButton setTitle:dateString forState:UIControlStateNormal];
    
    CALayer *arrivalLayer = [self.departureDateTimeButton layer];
    [arrivalLayer setMasksToBounds:YES];
    [arrivalLayer setCornerRadius:3.0f];
    
    CALayer *departurelayer = [self.arrivalDateTimeButton layer];
    [departurelayer setMasksToBounds:YES];
    [departurelayer setCornerRadius:3.0f];
}

-(NSDate *)roundDateToCeiling15Minutes {
    NSDate *mydate = [NSDate date];
    // Get the nearest 5 minute block
    NSDateComponents *time = [[NSCalendar currentCalendar]
                              components:NSCalendarUnitHour | NSCalendarUnitMinute
                              fromDate:mydate];
    NSInteger minutes = [time minute];
    int remain = minutes % 15;
    // Add the remainder of time to the date to round it up evenly
    mydate = [mydate dateByAddingTimeInterval:60*(15-remain)];
    return mydate;
}

- (IBAction)hideButton:(id)sender {
    [self.origin resignFirstResponder];
    [self.destination resignFirstResponder];
    if (self.departureDatePicker != nil)
    {
        [self.departureDatePicker removeFromSuperview];
    }
    if (self.arrivalDatePicker != nil)
    {
        [self.arrivalDatePicker removeFromSuperview];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"OUTBOUND_SEGUE"])
    {
        JourneyDateViewController *controller = [segue destinationViewController];
        controller.title = @"Out";
        controller.delegate = self;
        self.isOutward = TRUE;
    }
    else if ([[segue identifier] isEqualToString:@"INBOUND_SEGUE"])
    {
        JourneyDateViewController *controller = [segue destinationViewController];
        controller.title = @"Return";
        controller.delegate = self;
        self.isOutward = FALSE;
    }
}

- (void) handleDateTime:(NSDate*) date
{
    NSDateFormatter *formatter;
    NSString        *dateString;
    
    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"EEE dd MMM yyyy hh:mm aaa"];
    
    dateString = [formatter stringFromDate:date];
    
    if (self.isOutward)
    {
        [self.departureDateTimeButton setTitle:dateString forState:UIControlStateNormal];
    }
    else
    {
        [self.arrivalDateTimeButton setTitle:dateString forState:UIControlStateNormal];
    }
}

- (IBAction)segmentValueChanged:(id)sender {
    [self hideButton:sender];
    UISegmentedControl *segmentedControl = (UISegmentedControl *)sender;
    if ([segmentedControl selectedSegmentIndex] == 2)
    {
        self.arrivalDateTimeButton.hidden = NO;
    }
    else
    {
        self.arrivalDateTimeButton.hidden = YES;

    }
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    if ([identifier isEqualToString:@"ViewJourney"]) {
        NSString *errorMessage;
        NSString *origin = self.origin.text;
        NSString *destination = self.destination.text;
        
        NSDateFormatter *formatter= [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"EEE dd MMM yyyy hh:mm aaa"];
        NSDate *departureDate = [formatter dateFromString:self.departureDateTimeButton.titleLabel.text];
        
        if (origin.length == 0 || destination.length == 0) {
            errorMessage = @"Origin or destination cannot be empty";
        } else if ([origin isEqualToString:destination]) {
            errorMessage = @"Origin and destination cannot be same";
        } else if ([departureDate compare:[NSDate date]] == NSOrderedAscending) {
            errorMessage = @"Departure cannot be in the past";
        }
        else if ([self.journeyTypeSelector selectedSegmentIndex] == 2) {
            NSDate *arrivalDate = [formatter dateFromString:self.arrivalDateTimeButton.titleLabel.text];
            
            if ([arrivalDate compare:[NSDate date]] == NSOrderedAscending) {
                errorMessage = @"Arrival cannot be in the past";
            } else if ([arrivalDate compare:departureDate] == NSOrderedAscending) {
                errorMessage = @"Arrival cannot be before departure";
            }
        }
        
        if ([errorMessage length] != 0) {
            UIAlertView* alert=[[UIAlertView alloc] initWithTitle:@""
                                                          message:errorMessage
                                                         delegate:self cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
        
            [alert show];
            return NO;
        }
        return YES;
    }
    return YES;
}

@end
