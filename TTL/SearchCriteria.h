#import <Foundation/Foundation.h>

@interface SearchCriteria : NSObject
@property (nonatomic, strong) NSString *origin;
@property (nonatomic, strong) NSString *destination;
@property (nonatomic, strong) NSString *journeyType;
@property (nonatomic, strong) NSDate *departureDateTime;
@property (nonatomic, strong) NSDate *arrivalDateTime;
@end