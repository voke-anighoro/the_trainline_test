When(/^I click the ([^"]*) (?:date picker|button|field|drop down) on the navigation bar$/) do |attribute|
  @current_page.navigation_bar.send(attribute.snake_case).click
end

Then(/^I should see an alert message$/) do |message|
  # message is a Cucumber::MultilineArgument::DataTable
  expect(@current_page).to have_alert
  expect(@current_page.alert.message).to eq(message.raw.flatten.first)
end

When(/^I fill the ([^"]*) field with text "([^"]*)"$/) do |attribute, text|
  @current_page.send(attribute.snake_case).set(text)
end

When(/^I choose "([^"]*)" from the journey options$/) do |option|
  @current_page.journey_options.send(option.snake_case).click
end

Then(/^I should( not)? see the ([^"]*) (?:date picker|button|field|drop down)$/) do |negate, attribute|
  see = true
  see = false if negate
  expect(@current_page.send("has_#{attribute.snake_case}?")).to eq(see)
end

Then(/^I select (?:a|an) ([^"]*) date( \d+)?( [^"]*)? in the ([^"]*)$/) do |attribute, count, option, timeline|
  jump = get_jump(count: count, option: option)
  now = Time.now
  time = now + jump
  time = now - jump if timeline == 'past'
  date = time.strftime('%a %b %e')
  date = 'Today' if time.strftime('%a %b %e') == Time.now.strftime('%a %b %e')
  hour = time.strftime('%l')
  am_or_pm = time.strftime('%p')
  previous_page = @current_page
  previous_page.send(attribute.snake_case).click
  date_picker_page.select(date: date.strip, hour: hour.strip, am_or_pm: am_or_pm.strip)
  @current_page = previous_page
end

When(/^I click on any journey$/) do
  journeys_page.journeys.sample.root_element.click
end

Then(/^I (?:am|should be) on the ([^"]*)$/) do |page|
  send(page.snake_case)
end

Then(/^the navigation heading should have text "([^"]*)"$/) do |text|
  expect(@current_page.navigation_bar.heading.text).to eq(text)
end
