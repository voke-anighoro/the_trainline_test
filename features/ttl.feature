Feature: The Trainline.com Application
  As a user
  I want to find suitable train journey
  And to achieve this I type origin and destination, journey times and journey type (Single/Open return/Return)

  Background: All test should start at the homepage
    Given I am on the homepage

  Scenario: Origin field cannot be empty
    When I fill the destination field with text "London"
    And I click the search button on the navigation bar
    Then I should see an alert message
    | Origin or destination cannot be empty |

  Scenario: Destination field cannot be empty
    When I fill the origin field with text "France"
    And I click the search button on the navigation bar
    Then I should see an alert message
    | Origin or destination cannot be empty |

  Scenario Outline: User should should be able to book a single or open return journey successfully
    When I fill the origin field with text "London Victoria"
    And I fill the destination field with text "Paris"
    And I choose "<journey_option>" from the journey options
    Then I should not see the arrival date picker
    When I select a departure date in the future
    And I click the search button on the navigation bar
    Then I should be on the journeys page
    When I click on any journey
    Then I should be on the done page
    And the navigation heading should have text "Done!"
    Examples: Journey Options
      | journey_option |
      | one way        |
      | open return    |

  Scenario: User should not be able to book a departure in the past
    When I fill the origin field with text "London Victoria"
    And I fill the destination field with text "Paris"
    And I choose "one way" from the journey options
    And I select a departure date in the past
    And I click the search button on the navigation bar
    Then I should see an alert message
    | Departure cannot be in the past |

  Scenario: User should not be able to book a journey where the return date is less than the departure date or in the past
    When I fill the origin field with text "London Victoria"
    And I fill the destination field with text "Paris"
    And I choose "return" from the journey options
    And I select a departure date in the future
    And I select an arrival date in the past
    And I click the search button on the navigation bar
    Then I should see an alert message
    | Arrival cannot be in the past |

  Scenario: User should should be able to book a return journey successfully
    When I fill the origin field with text "London Victoria"
    And I fill the destination field with text "Paris"
    And I choose "return" from the journey options
    Then I should see the arrival date picker
    When I select a departure date in the future
    And I select an arrival date 2 days in the future
    And I click the search button on the navigation bar
    Then I should be on the journeys page
    When I click on any journey
    Then I should be on the done page
    And the navigation heading should have text "Done!"
