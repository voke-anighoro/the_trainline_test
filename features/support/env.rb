require 'bundler'
require 'capybara/dsl'

PROJECT_ROOT = File.expand_path(File.join(File.dirname(__FILE__), '..', '..'))
$LOAD_PATH << File.join(PROJECT_ROOT, 'lib')

Bundler.require

require_all 'lib'
require 'device'

Capybara.configure do |config|
  config.match = :prefer_exact
  config.run_server = false
  config.default_driver = :appium
  config.default_max_wait_time = 1
end

SitePrism.configure do |config|
  config.use_implicit_waits = true
end

World(Capybara::DSL)
