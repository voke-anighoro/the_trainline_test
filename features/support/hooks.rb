Before do
  Device.start_server
  Capybara.current_session.driver.browser
end

After do
  Capybara.current_session.driver.quit
  Device.stop_server
end

at_exit do
  Device.stop_server if defined?(Device)
end
